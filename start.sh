#!/bin/bash

#start gunicorn process
echo Starting Gunicorn.
exec gunicorn fuapi.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3

