from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class User(AbstractUser):
    phone = models.CharField(max_length=25)


class Category(models.Model):

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    name = models.CharField(max_length=50, blank=False, null=True)
    description = models.TextField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to='images/Categories/%m/%d')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Subcategory(models.Model):

    class Meta:
        verbose_name = "Subcategory"
        verbose_name_plural = "Subcategories"

    name = models.CharField(max_length=50, blank=True, null=True)
    category = models.ForeignKey(
        Category, related_name='subcategories', on_delete=models.CASCADE)
    description = models.TextField()
    image = models.ImageField(upload_to='images/Subcategories/%m/%d')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Offer(models.Model):
    OFF = 'OFF'
    ON = 'ON'
    OFFER_STATUS = (
        (ON, 'ONGOING'),
        (OFF, 'SUSPENDED')
    )

    class Meta:
        verbose_name = "Offer"
        verbose_name_plural = "Offers"

    name = models.CharField(max_length=50, blank=True, null=True)
    value = models.IntegerField()
    status = models.CharField(max_length=3, choices=OFFER_STATUS, default=OFF)
    date_start = models.DateField()
    date_end = models.DateField()

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def discount(self):
        if self.OFFER_STATUS == 'ON':
            Item.price = Item.price - self.value
        else:
            Item.price = Item.price


class Item(models.Model):

    class Meta:
        verbose_name = "Item"
        verbose_name_plural = "Items"

    name = models.CharField(max_length=50, blank=True, null=True)
    subcategory = models.ForeignKey(Subcategory, on_delete=models.CASCADE)
    offer = models.ForeignKey(Offer)
    price = models.IntegerField()

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Order(models.Model):

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ManyToManyField(Item, related_name='orders')
    total = models.DecimalField()
    quantity = models.IntegerField()
    pick_up_date = models.DateField()
    pick_up_time = models.TimeField()
    delivery_date = models.DateField()
    delivery_time = models.TimeField()
    delivery_note = models.TextField()

    @property
    def total(self):
        result = self.items.aggregate(Sum('price'))
        return result['total']
