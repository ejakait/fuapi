from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_auth.serializers import UserDetailsSerializer
from .models import Offer, Category, Subcategory, Order, Item


class UserSerializer(UserDetailsSerializer):
    class Meta:
        User = get_user_model()
        model = User
        fields = UserDetailsSerializer.Meta.fields + ('phone',)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name', 'description', 'image', )


class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = ('name', 'price', 'subcategory', 'offer', )


class SubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategory
        fields = ('name', 'category', 'description', 'image',)


class OfferSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = ('name', 'value', 'status',)


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('__all__')
