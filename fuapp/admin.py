from django.contrib import admin
from .models import User, Order, Category, Subcategory, Item, Offer


# Register your models here.
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email')


admin.site.register(User, UserAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


admin.site.register(Category, CategoryAdmin)


class SubcategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


admin.site.register(Subcategory, SubcategoryAdmin)


class ItemAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(Item, ItemAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'pick_up_date',
                    'delivery_date', 'delivery_note')


admin.site.register(Order, OrderAdmin)


class OfferAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', )


admin.site.register(Offer, OfferAdmin)
